#!/bin/bash
#
# Copyright 2023 Guido Trotter <ultrotter@quaqua.net>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

set -e

host="${1:?Usage: $0 hostname}"

autosetupd=$(dirname $0)

. ${autosetupd}/utils

# load default config, and any override
. ${autosetupd}/config
conf_override=${CONF:-${autosetupd}/config}
. ${conf_override}

destd="${DESTDIR:-/var/lib/libvirt/images}"
tmpd=$(mktemp -d)

mode=${MODE:-cidata}
cidata_disk=""
if [ "${mode}" = "cidata" ]; then
  DESTDIR=${destd} CONF=${CONF} ${autosetupd}/setup-create-cidataiso ${host}
  host=${host} imgdir=${destd} envsubst < ${autosetupd}/libvirt-cidata.xml.in > ${tmpd}/${host}-cidata-conf.xml
  cidata_disk=$(cat ${tmpd}/${host}-cidata-conf.xml)
fi

DESTDIR=${destd} CONF=${CONF} ${autosetupd}/setup-create-image ${host}

swap_mode=${SWAP_MODE:-dd}
swap_size=${SWAP_SIZE:-1024}
swap_label=${SWAP_LABEL:-ASWVOL}

make_volume ${swap_mode} ${destd}/${host}-swap.img ${swap_size} swap ${swap_label}

swap_disk=""
if [ "${swap_mode}" != "none" ]; then
  host=${host} imgdir=${destd} envsubst < ${autosetupd}/libvirt-swap.xml.in > ${tmpd}/${host}-swap-conf.xml
  swap_disk=$(cat ${tmpd}/${host}-swap-conf.xml)
fi

storage_mode=${STORAGE_MODE:-truncate}
storage_disk=""
if [ "${storage_mode}" != "none" ]; then
  host=${host} imgdir=${destd} envsubst < ${autosetupd}/libvirt-storage.xml.in > ${tmpd}/${host}-storage-conf.xml
  storage_disk=$(cat ${tmpd}/${host}-storage-conf.xml)
fi

host=${host} imgdir=${destd} cidata_disk=${cidata_disk} storage_disk=${storage_disk} swap_disk=${swap_disk} envsubst < ${autosetupd}/libvirt-domain.xml.in > ${tmpd}/${host}-conf.xml

# create is not persistent:
# virsh create ${tmpd}/${host}-conf.xml

virsh define ${tmpd}/${host}-conf.xml
virsh start ${host}

rm -rf ${tmpd}
